///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.h
/// @version 1.0
///
/// Exports data about cats
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   26 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>
#include "animals.h"

void addAliceTheCat();
void printCat();

/// Declare a struct Cat
struct Cat {
   char name[30];
   enum Gender gender;
   enum CatBreeds breed;
   bool isFixed;
   float weight;
   enum Color collar1_color;
   enum Color collar2_color;
   long license;
};
