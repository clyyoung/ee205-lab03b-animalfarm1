///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   26 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"
#include "animals.h"

// Declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat catDB[MAX_SPECIES];

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
void addAliceTheCat(int i) {
   strcpy ( catDB[0].name, "Alice" );
   catDB[0].gender = FEMALE;
   catDB[0].breed = MAIN_COON;
   catDB[0].isFixed = true;
   catDB[0].weight = 12.34;
   catDB[0].collar1_color = BLACK;
   catDB[0].collar2_color = RED;
   catDB[0].license = 12345;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   printf ("Cat name = [%s]\n", catDB[i].name);
   printf ("    gender = [%s]\n", gender( catDB[i].gender ));
   printf ("    breed = [%s]\n", breedName( catDB[i].breed ));
   printf ("    isFixed = [%s]\n", fixed( catDB[i].isFixed ));
   printf ("    weight = [%0.2f]\n", catDB[i].weight );
   printf ("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf ("    collar color 2 = [%s]\n", colorName( catDB[i].collar2_color ));
   printf ("    license = [%ld]\n", catDB[i].license );
}

