///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   26 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

#include <stdbool.h>

/// Create enums for fields appropriate for all animals
enum Gender {MALE, FEMALE};
enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};
enum CatBreeds {MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

/// Return a string for the color, breed, gender, and isFixed
/// Changing enums to strings
char* colorName (enum Color color);
char* breedName (enum CatBreeds breed);
char* gender    (enum Gender gender);
char* fixed     (bool isFixed); 
