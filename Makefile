###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Christianne Young <clyyoung@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   26 Jan 2021
###############################################################################

# target is animalfarm
all: animalfarm


# all executables and their dependencies
main.o: main.c cat.h animals.h
	gcc -c main.c

cat.o: cat.c cat.h animals.h
	gcc -c cat.c

animals.o: animals.c animals.h cat.h
	gcc -c animals.c

animalfarm: main.o cat.o animals.o
	gcc -o animalfarm main.o cat.o animals.o

clean:
	rm -f *.o animalfarm
