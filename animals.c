///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Christianne Young <clyyoung@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   26 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {
   // Map the enum Color to a string
   switch(color){
      case BLACK: return "Black";
                  break;
      case WHITE: return "White";
                  break;
      case RED:   return "Red";
                  break;
      case BLUE:  return "Blue";
                  break;
      case GREEN: return "Green";
                  break;
      case PINK:  return "Pink";
                  break;
   }
   return NULL; // We should never get here
};

/// Decode the enum CatBreeds into strings for printf()
char* breedName (enum CatBreeds breed) {
   // Map the enum CatBreeds to a string
   switch(breed){
      case MAIN_COON:   return "Main Coon";
                        break;
      case MANX:        return "Manx";
                        break;
      case SHORTHAIR:   return "Shorthair";
                        break;
      case PERSIAN:     return "Persian";
                        break;
      case SPHYNX:      return "Sphynx";
                        break;
   }
   return NULL; // We should never get here
};

/// Decode the enum Gender into strings for printf()
char* gender (enum Gender gender) {
   // Map the enum Gender to a string
   switch(gender){
      case MALE:     return "Male";
                     break;
      case FEMALE:   return "Female";
                     break;
   }
   return NULL; // We should never get here
};

/// Decode the bool isFixed into strings for printf()
char* fixed (bool isFixed) {
   // Map the bool isFixed to a string
   switch(isFixed){
      case true:  return "Yes";
                  break;
      case false: return "No";
                  break;
   }
   return NULL; // We should never get here
};


